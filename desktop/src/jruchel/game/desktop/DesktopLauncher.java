package jruchel.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import java.util.concurrent.ScheduledThreadPoolExecutor;

import jruchel.game.MainGame;
import jruchel.game.output.AlertDisplayer;

class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        final MainGame listener = new MainGame();
        /*final HanoiTowerBot bot = new HanoiTowerBot(listener);
        listener.setPlatform(MainGame.Platform.PC);*/

        listener.setAlertDisplayer(new AlertDisplayer() {
            @Override
            public void display(String msg) {
                System.out.println(msg);
            }
        });
        new LwjglApplication(listener, config);

        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);

      //  executor.schedule(bot, 2, TimeUnit.SECONDS);
    }
}
