package jruchel.game.containers;

import java.util.ArrayList;
import java.util.List;

public class Stack<T> {

    private List<T> list;

    public Stack() {
        list = new ArrayList<>();
    }

    public Stack(List<T> elements) {
        this();
        list.addAll(elements);
    }

    public int size() {
        return list.size();
    }

    public boolean empty() {
        return list.isEmpty();
    }

    public T peek() {
        if (!empty()) {
            return list.get(list.size() - 1);
        } else {
            return null;
        }
    }

    public T pop() {
        T temp = list.get(list.size() - 1);
        list.remove(list.size() - 1);
        return temp;
    }

    public void push(T element) {
        list.add(element);
    }

    public List<T> getAsList() {
        return list;
    }
}
