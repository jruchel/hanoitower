package jruchel.game.rendering;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;
import java.util.List;

public class Renderer implements Disposable {

    private final SpriteBatch batch;
    private final List<Renderable> textures;
    private final List<RenderableText> texts;
    private final List<Renderable> renderables;

    public Renderer(SpriteBatch batch) {
        this.batch = batch;
        this.textures = new ArrayList<>();
        this.texts = new ArrayList<>();
        this.renderables = new ArrayList<>();
    }

    public void render() {

        batch.begin();

        List<Renderable> tempRenderables = new ArrayList<>();

        tempRenderables.addAll(textures);

        tempRenderables.addAll(texts);

        for (Renderable r : tempRenderables) {
            r.render(batch);
        }

        batch.end();
    }

    public void addRenderables(List<? extends Renderable> renderables) {
        for (Renderable r : renderables) {
            addRenderable(r);
        }
    }

    public void addRenderable(Renderable renderable) {

        if (renderable instanceof RenderableText) {
            texts.add((RenderableText) renderable);
        }
        if (renderable instanceof RenderableObject) {
            textures.add(renderable);
        }
        renderables.add(renderable);
    }

    private void flushTexts() {
        texts.clear();
    }

    private void flushTextures() {
        textures.clear();
    }

    private void flushRenderables() {
        renderables.clear();
    }

    private void flush() {
        flushRenderables();
        flushTexts();
        flushTextures();
    }

    public List<RenderableText> getTexts() {
        return texts;
    }

    public List<Renderable> getRenderables() {
        return renderables;
    }

    public List<Renderable> getTextures() {
        return textures;
    }

    @Override
    public void dispose() {
        batch.dispose();
        flush();
    }
}
