package jruchel.game.rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import jruchel.game.click_handling.Clickable;
import jruchel.game.click_handling.OnClickListener;

public class RenderableText implements Renderable, Clickable {

    private final FreeTypeFontGenerator fontGenerator;
    private final float x, y;
    private FreeTypeFontGenerator.FreeTypeFontParameter fontParameter;
    private BitmapFont font;
    private String text;
    private OnClickListener onClickListener;

    public RenderableText(FreeTypeFontGenerator.FreeTypeFontParameter fontParameter, String text, float x, float y) {
        this.text = text;
        this.fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/BEARPAW_.ttf"));
        this.fontParameter = fontParameter;
        this.font = fontGenerator.generateFont(fontParameter);
        this.x = x;
        this.y = y;
    }

    public RenderableText(String text, float x, float y) {
        this(new FreeTypeFontGenerator.FreeTypeFontParameter(), text, x, y);
        this.fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontParameter.size = 15;
        fontParameter.color = Color.BLACK;
        fontParameter.padBottom = 3;
        fontParameter.padLeft = 3;
        fontParameter.padRight = 3;
        fontParameter.padTop = 3;
        this.font = fontGenerator.generateFont(fontParameter);
    }

    @Override
    public String toString() {
        return text;
    }

    public FreeTypeFontGenerator.FreeTypeFontParameter getFontParameter() {
        return fontParameter;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void render(SpriteBatch batch) {
        font.draw(batch, text, x, y);
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        this.onClickListener = listener;
    }

    @Override
    public void onClick() {
        onClickListener.onClick(this);
    }

    @Override
    public boolean isWithin(float x, float y) {
        return false;
    }

}
