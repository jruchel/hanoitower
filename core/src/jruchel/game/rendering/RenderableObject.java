package jruchel.game.rendering;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Locale;

import jruchel.game.click_handling.Clickable;
import jruchel.game.click_handling.OnClickListener;

public class RenderableObject extends Actor implements Renderable, Clickable {

    private Texture texture;
    private OnClickListener onClickListener;

    public RenderableObject(Texture texture, int x, int y, int width, int height) {
        this.texture = texture;
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
    }


    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }


    protected Vector2 getCenterCords() {
        Vector2 cords = new Vector2();
        cords.x = this.getX() + this.getWidth() / 2;
        cords.y = this.getY() + this.getHeight() / 2;
        return cords;
    }

    /**
     * A method used in the Renderer class, defines how an object should be rendered
     *
     * @param batch Batch from Renderer class to be used to draw the object
     */
    @Override
    public void render(SpriteBatch batch) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        this.onClickListener = listener;
    }

    @Override
    public void onClick() {
        onClickListener.onClick(this);
    }

    public void updateCords(float x, float y) {
        setX(x);
        setY(y);
    }

    @Override
    public boolean isWithin(float x, float y) {
        //Checking if the x value is within object bounds
        if (x >= getX() && x <= getX() + getWidth()) {
            //Checking if the y value is within object bounds
            return y >= getY() && y <= (getY() + getHeight());
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "X: %f Y: %f", getX(), getY());
    }
}
