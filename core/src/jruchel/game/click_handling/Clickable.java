package jruchel.game.click_handling;

public interface Clickable {
    void setOnClickListener(OnClickListener listener);
    void onClick();
    boolean isWithin(float x, float y);
}
