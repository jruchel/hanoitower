package jruchel.game.click_handling;

import jruchel.game.rendering.Renderable;

public interface OnClickListener {
    void onClick(Renderable r);
}
