package jruchel.game.output;

public interface AlertDisplayer {
    void display(String msg);
}
