package jruchel.game.repositories;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class Preferences implements com.badlogic.gdx.Preferences {

    private static Preferences instance;
    private String path;
    private Properties props;

    private Preferences() throws IOException, NullPointerException {
        this.path = "preferences.properties";
        InputStream input = ClassLoader.getSystemResourceAsStream(path);
        props = new Properties();
        try {
            props.load(input);
        } catch (NullPointerException ignored) {
        }

    }

    public static Preferences getInstance() throws IOException {
        return instance == null ? instance = new Preferences() : instance;
    }

    @Override
    public com.badlogic.gdx.Preferences putBoolean(String key, boolean val) {
        return this;
    }

    @Override
    public com.badlogic.gdx.Preferences putInteger(String key, int val) {
        props.setProperty(key, String.valueOf(val));
        return this;
    }

    @Override
    public com.badlogic.gdx.Preferences putLong(String key, long val) {
        return this;
    }

    @Override
    public com.badlogic.gdx.Preferences putFloat(String key, float val) {
        return this;
    }

    @Override
    public com.badlogic.gdx.Preferences putString(String key, String val) {
        props.setProperty(key, val);
        return this;
    }

    @Override
    public com.badlogic.gdx.Preferences put(Map<String, ?> vals) {
        return this;
    }

    @Override
    public boolean getBoolean(String key) {
        return false;
    }

    @Override
    public int getInteger(String key) {
        return Integer.parseInt(props.getProperty(key));
    }

    @Override
    public long getLong(String key) {
        return 0;
    }

    @Override
    public float getFloat(String key) {
        return 0;
    }

    @Override
    public String getString(String key) {
        return props.getProperty(key);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return false;
    }

    @Override
    public int getInteger(String key, int defValue) {
        return props.getProperty(key) == null ? defValue : getInteger(key);
    }

    @Override
    public long getLong(String key, long defValue) {
        return 0;
    }

    @Override
    public float getFloat(String key, float defValue) {
        return 0;
    }

    @Override
    public String getString(String key, String defValue) {
        return null;
    }

    @Override
    public Map<String, ?> get() {
        return null;
    }

    @Override
    public boolean contains(String key) {
        return props.containsKey(key);
    }

    @Override
    public void clear() {
        props.clear();
    }

    @Override
    public void remove(String key) {
        props.setProperty(key, null);
    }

    @Override
    public void flush() {
    }
}
