package jruchel.game.configuration;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import jruchel.game.exceptions.ConfigAccessFailureException;

public class Configuration {

    private static Configuration instance;
    private static Properties prop;

    private Configuration() throws ConfigAccessFailureException {

        String filename = "config.properties";
        InputStream input = ClassLoader.getSystemResourceAsStream(filename);

        if (input == null) {
            throw new ConfigAccessFailureException();
        }

        prop = new Properties();

        try {
            prop.load(input);
        } catch (IOException e) {
            throw new ConfigAccessFailureException();
        }

    }

    public static Configuration getInstance() throws ConfigAccessFailureException {
        return instance == null ? instance = new Configuration() : instance;
    }

}
