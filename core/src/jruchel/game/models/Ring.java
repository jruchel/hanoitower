package jruchel.game.models;

import com.badlogic.gdx.graphics.Texture;

import jruchel.game.rendering.RenderableObject;

public class Ring extends RenderableObject {

    final int size;

    public Ring(Texture texture, int size) {
        this(texture, 25 * size, 20, size);
    }

    public Ring(Texture texture, int width, int height, int size) {
        this(texture, 0, 0, width, height, size);
    }

    public Ring(Texture texture, int x, int y, int width, int height, int size) {
        super(texture, x, y, width * size, height);
        this.size = size;
    }

}
