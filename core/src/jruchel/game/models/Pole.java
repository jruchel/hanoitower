package jruchel.game.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Locale;
import java.util.Objects;

import jruchel.game.containers.Stack;
import jruchel.game.rendering.RenderableObject;

public class Pole extends RenderableObject {

    private Stack<Ring> rings;

    public Pole(Texture texture, int x, int y) {
        this(texture, x, y, 0, 0);
    }

    public Pole(Texture texture, int x, int y, int width, int height) {
        super(texture, x, y, width, height);
        rings = new Stack<>();
    }

    /**
     * Moves a ring from one pole to another
     *
     * @param p The pole to move to
     * @return Whether the ring was moved
     */
    public boolean moveRing(Pole p) {
        //If the poles are the same returns false
        if (this.equals(p)) {
            return false;
        }
        //If the current pole is empty returns false
        if (this.rings.empty()) {
            return false;
        }
        //If the current pole's top ring is bigger than target pole's returns false
        if (!p.rings.empty() && p.rings.peek().size < rings.peek().size) {
            return false;
        }
        //If all the above conditions are not met moves the ring
        else {
            Ring temp = rings.pop();
            p.addRing(temp);
            return true;
        }
    }

    public boolean isEmpty() {
        return rings.empty();
    }

    public Stack<Ring> getRings() {
        return rings;
    }

    public void setRings(Stack<Ring> rings) {
        this.rings = rings;
    }

    public void addRing(Ring r) {
        r.setX(this.getCenterCords().x - r.getWidth() / 2);
        r.setY((r.getHeight() + 5) * rings.size() + 1);
        this.rings.push(r);
    }

    @Override
    public void render(SpriteBatch batch) {
        super.render(batch);
        for (Ring r : rings.getAsList()) {
            r.render(batch);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Pole pole = (Pole) o;
        return Objects.equals(rings, pole.rings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), rings);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "X: %f, Y: %f", getX(), getY());
    }
}
