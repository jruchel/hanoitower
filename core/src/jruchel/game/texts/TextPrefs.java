package jruchel.game.texts;

import java.util.Map;

public class TextPrefs {

    private static TextPrefs instance;
    private Map<String, String> texts;
    private Map<String, String> defaults;

    public static TextPrefs getInstance() {
        if (instance == null) {
            instance = new TextPrefs();
        }
        return instance;
    }

    public void setTexts(Map<String, String> texts) {
        this.texts = texts;
    }

    public void setDefaults(Map<String, String> texts) {
        defaults = texts;
    }

    public void addDefault(String key, String val) {
        defaults.put(key, val);
    }

    public void addText(String key, String val) {
        texts.put(key, val);
    }

    public String get(String key) {
        try {
            return texts.get(key);
        } catch (NullPointerException ex) {
            return defaults.get(key);
        }
    }
}
