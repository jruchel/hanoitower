package jruchel.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jruchel.game.output.AlertDisplayer;
import jruchel.game.rendering.Renderable;
import jruchel.game.rendering.Renderer;
import jruchel.game.screens.TitleScreen;
import jruchel.game.texts.TextPrefs;

public class MainGame extends Game {

    private SpriteBatch batch;

    //Output
    private AlertDisplayer alertDisplayer;

    private Renderer renderer;

    private float screenWidth;
    private float screenHeight;

    //Texts
    private TextPrefs textPrefs = TextPrefs.getInstance();

    @Override
    public void create() {
        batch = new SpriteBatch();
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        renderer = new Renderer(new SpriteBatch());

        createTextPrefsDefaults();

        setScreen(new TitleScreen(this));
    }

    public void resetRenderer() {
        if (renderer != null) {
            dispose();
        }
        renderer = new Renderer(new SpriteBatch());
    }

    public TextPrefs getTextPrefs() {
        return textPrefs;
    }

    public void setTextPrefs(TextPrefs textPrefs) {
        this.textPrefs = textPrefs;
    }

    public void setTextPrefs(Map<String, String> texts) {
        textPrefs.setTexts(texts);
    }

    public void renderTextures() {
        renderer.render();
    }

    public void addRenderables(List<? extends Renderable> renderables) {
        renderer.addRenderables(renderables);
    }

    public void addRenderable(Renderable renderable) {
        renderer.addRenderable(renderable);
    }

    public AlertDisplayer getAlertDisplayer() {
        return alertDisplayer;
    }

    public void setAlertDisplayer(AlertDisplayer alertDisplayer) {
        this.alertDisplayer = alertDisplayer;
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public float getScreenHeight() {
        return screenHeight;
    }

    public float getScreenWidth() {
        return screenWidth;
    }


    @Override
    public void dispose() {
    }

    private void createTextPrefsDefaults() {
        Map<String, String> defaults = new HashMap<>();
        defaults.put("app_name", "Tower of Hanoi");
        defaults.put("gameOver", "%d/%d moves - Score: %d");
        defaults.put("moves", "Time : %.1f Moves: %d");
        defaults.put("newHS", "New Highscore!");
        defaults.put("highScore", "Current Highscore: %d");
        defaults.put("HSSaved", "Highscore saved");
        defaults.put("time", "Time: %.1f Moves: %d");
        defaults.put("selectRings", "Tap a number key to select number of rings");
        setTextPrefsDefaults(defaults);
    }

    private void setTextPrefsDefaults(Map<String, String> texts) {
        textPrefs.setDefaults(texts);
    }

}

//TODO! instead of giving screens whole objects only give them the methods they will need
//TODO! Make a way of selecting rings on phones
//TODO! make the renderer and similar classes only exist in game class and create class api to access them elsewhere


//TODO Fix bug with the game starting after resetting if before pressing space on pc other keys were pressed
//TODO Fix the glitch with the game always starting when it was minimized, even if no moves were made yet
//TODO Adjust touch detections when screen parameters change(Maybe try checking of the click happened was less than a third of the width for example and half the height)
//TODO Center game over text
//TODO Save high score on the device instead of cache
//DONE Resume the game if a move was made when paused
//TODO fix time not being counted properly