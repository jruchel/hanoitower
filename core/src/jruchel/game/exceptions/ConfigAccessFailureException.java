package jruchel.game.exceptions;

public class ConfigAccessFailureException extends Exception {

    public ConfigAccessFailureException() {
        super("Failure accessing the config file");
    }

    public ConfigAccessFailureException(String m) {
        super(m);
    }

}
