package jruchel.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import jruchel.game.MainGame;
import jruchel.game.click_handling.Clickable;
import jruchel.game.click_handling.OnClickListener;
import jruchel.game.models.Pole;
import jruchel.game.models.Ring;
import jruchel.game.output.AlertDisplayer;
import jruchel.game.rendering.Renderable;
import jruchel.game.rendering.RenderableText;
import jruchel.game.repositories.Preferences;
import jruchel.game.texts.TextPrefs;

public class GameScreen implements Screen {

    private MainGame game;

    private float screenHeight;
    private float screenWidth;

    private GameState gameState = GameState.NOT_STARTED;

    //Variables
    private int moves;
    private int rings;
    private int highScore;
    private long currentTime;
    private long startTime;
    private Platform platform = Platform.DEFAULT;

    //Rendering
    private Texture poleTexture;
    private Texture ringTexture;

    //Texts
    private RenderableText highScoreText;
    private RenderableText gameOverText;
    private RenderableText movesText;
    private TextPrefs textPrefs;

    //Models
    private Pole currentPole;
    private Pole targetPole;

    //Listeners
    private List<Clickable> clickables;
    private List<Pole> poles;
    private AlertDisplayer alertDisplayer;

    GameScreen(MainGame game, int rings) {
        this.game = game;
        this.rings = rings;
        this.screenHeight = game.getScreenHeight();
        this.screenWidth = game.getScreenWidth();
        this.alertDisplayer = game.getAlertDisplayer();
        this.textPrefs = game.getTextPrefs();
    }

    @Override
    public void show() {
        game.resetRenderer();
        setGame(rings);
        readHighScore();
        assignInputProcessor();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.renderTextures();
        movesText.setText(String.format(Locale.US, textPrefs.get("time"), getCurrentTime() / 10, moves));
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        gameState = GameState.RUNNING;
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    /**
     * Decides which control scheme to use
     */
    private void assignInputProcessor() {
        switch (platform) {
            case PC:
                Gdx.input.setInputProcessor(new DesktopInputHandler());
                break;
            case ANDROID:
                Gdx.input.setInputProcessor(new GestureDetector(new PhoneInputHandler()));
                break;
            default:
                Gdx.input.setInputProcessor(new DesktopInputHandler());
                break;
        }
    }

    public void setAlertDisplayer(AlertDisplayer alertDisplayer) {
        this.alertDisplayer = alertDisplayer;
    }

    private void createPoles() {

        poles = new ArrayList<>();

        int width = Gdx.graphics.getWidth();

        //Calculate pole width and height based on screen size
        int poleHeight = Gdx.graphics.getHeight() / 2;
        int poleWidth = Gdx.graphics.getWidth() / 6;

        //Adding click listeners
        Pole pole1 = new Pole(poleTexture, ((width / 4) - (poleWidth / 2)), 0, poleWidth, poleHeight);
        Pole pole2 = new Pole(poleTexture, ((width / 2) - (poleWidth / 2)), 0, poleWidth, poleHeight);
        Pole pole3 = new Pole(poleTexture, ((3 * width / 4) - (poleWidth / 2)), 0, poleWidth, poleHeight);

        pole1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(Renderable r) {
                if (currentPole == null && !((Pole) r).isEmpty()) {
                    currentPole = (Pole) r;
                } else if (currentPole != null && targetPole == null && currentPole != (Pole) r) {
                    targetPole = (Pole) r;
                }
            }
        });
        pole2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(Renderable r) {
                if (currentPole == null && !((Pole) r).isEmpty()) {
                    currentPole = (Pole) r;
                } else if (currentPole != null && targetPole == null && currentPole != (Pole) r) {
                    targetPole = (Pole) r;
                }
            }
        });
        pole3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(Renderable r) {
                if (currentPole == null && !((Pole) r).isEmpty()) {
                    currentPole = (Pole) r;
                } else if (currentPole != null && targetPole == null && currentPole != (Pole) r) {
                    targetPole = (Pole) r;
                }
            }
        });

        poles.add(pole1);
        poles.add(pole2);
        poles.add(pole3);

        game.addRenderables(poles);
        clickables.addAll(poles);
    }

    private void createRings(int rings, int pole) {

        int width = (int) (game.getScreenWidth() / 25);
        int height = (int) (game.getScreenHeight() / 25);

        for (int i = rings; i > 0; i--) {
            poles.get(pole).addRing(new Ring(ringTexture, width, height, i));
        }
    }

    private void createAssets() {
        poleTexture = new Texture("poletexture.png");
        ringTexture = new Texture("ringTexture.png");

        createPoles();
        createRings(rings, 0);
        createTexts();
    }

    private RenderableText createHighScoreText() {
        FreeTypeFontGenerator.FreeTypeFontParameter fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontParameter.size = (int) (game.getScreenHeight() / 10);
        fontParameter.color = Color.BLACK;
        return highScoreText = new RenderableText(fontParameter, String.format(Locale.US, game.getTextPrefs().get("highScore"), highScore), game.getScreenWidth() - (fontParameter.size * 7), game.getScreenHeight());
    }

    private RenderableText createMovesText() {
        FreeTypeFontGenerator.FreeTypeFontParameter fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontParameter.size = (int) (screenHeight / 10);
        fontParameter.color = Color.BLACK;
        return movesText = new RenderableText(fontParameter, String.format(Locale.US, game.getTextPrefs().get("moves"), getCurrentTime() / 10, moves), 0, screenHeight);
    }

    private RenderableText createGameOverText() {
        FreeTypeFontGenerator.FreeTypeFontParameter fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontParameter.size = (int) (screenHeight / 10);
        fontParameter.color = Color.BLACK;
        String text = "";

        return new RenderableText(fontParameter, text, ((screenWidth / 2) - (fontParameter.size * 2)), ((screenHeight / 2) + (fontParameter.size / 2f)));
    }

    private void createTexts() {
        movesText = createMovesText();
        game.addRenderable(movesText);
        highScoreText = createHighScoreText();
        game.addRenderable(highScoreText);
        gameOverText = createGameOverText();
    }

    /**
     * Reads the current device saved highscore
     */
    private void readHighScore() {
        try {
            highScore = Preferences.getInstance().getInteger("highScore", 0);
        } catch (IOException e) {
            highScore = 0;
        }
    }

    /**
     * Sets the game state, resets the necessary assets resets variables
     *
     * @param rings The amount of rings to create for the game
     */
    private void setGame(int rings) {
        moves = 0;
        clickables = new ArrayList<>();

        game.resetRenderer();

        gameState = GameState.NOT_STARTED;
        currentTime = 0;

        startTime = TimeUtils.millis();

        this.rings = rings;

        createAssets();
    }

    private void performClicks(float x, float y) {
        y = Gdx.graphics.getHeight() - y;
        for (Clickable c : clickables) {
            if (c.isWithin(x, y)) {
                c.onClick();
                updateState();
                break;
            }
        }
        clickOnBackground();
        updateState();
    }

    /**
     * Method responsible for all the game logic
     */
    private void updateState() {
        if (gameState.equals(GameState.FINISHED)) {
            return;
        }
        //When the game is supposed to start
        if (currentPole != null && targetPole != null && gameState.equals(GameState.NOT_STARTED)) {
            resume();
        }
        //If the game is still paused don't do anything
        if (gameState.equals(GameState.NOT_STARTED)) {
            return;
        }
        //When different poles were clicked
        if (currentPole != null && targetPole != null) {
            if (currentPole.moveRing(targetPole)) {
                moves++;
                if (!gameState.equals(GameState.RUNNING)) {
                    resume();
                }
            }
            currentPole = null;
            targetPole = null;
        }
        if (isOver()) {
            gameOverText.setText(String.format(Locale.US, game.getTextPrefs().get("gameOver"), moves, getMinimumMoves(), calculateScore()));
            currentPole = null;
            targetPole = null;
            setFinished();
            //Saving the high score
            try {
                updateHighScore();
            } catch (Exception e) {
                if (alertDisplayer != null) {
                    alertDisplayer.display(e.getMessage());
                }
            }
        }
    }

    private void updateHighScore() throws IOException {
        int score;
        if ((score = calculateScore()) > highScore) {
            if (alertDisplayer != null) {
                alertDisplayer.display(game.getTextPrefs().get("newHS"));
            }

            highScore = score;
            Preferences.getInstance().putInteger("highScore", highScore);
            if (alertDisplayer != null) {
                alertDisplayer.display(game.getTextPrefs().get("HSSaved"));
            }
        }
    }

    public void clickOnBackground() {
        if (gameState.equals(GameState.FINISHED)) {
            reset();
        }
    }

    public void clickOnPole(Pole pole) {
        pole.onClick();
    }

    private void reset() {
        setGame(rings);
    }

    public boolean isOver() {
        return poles.get(2).getRings().size() == rings;
    }

    private int getMinimumMoves() {
        return (((int) Math.pow(2, rings)) - 1);
    }

    private int calculateScore() {
        int score = (int) ((10 - moves + getMinimumMoves()) + (getMinimumMoves() - getCurrentTime() / 10));
        return score <= 0 ? 0 : score;
    }

    private float getCurrentTime() {
        if (!gameState.equals(GameState.RUNNING)) {
            return currentTime;
        }
        return currentTime = TimeUtils.timeSinceMillis(startTime) / 100;
    }

    private void setFinished() {
        gameState = GameState.FINISHED;
    }

    /**
     * Switches the state of the game between two different states Running and Paused
     *
     * @return Whether the game state was changed or not
     */
    private boolean switchGameState() {
        switch (gameState) {
            case RUNNING:
                pause();
                return true;
            case PAUSED:
                resume();
                return true;
        }
        return false;
    }

    private void performKeyStrokes(int keycode) {
        switch (keycode) {
            case Input.Keys.NUM_1:
                clickOnPole(poles.get(0));
                break;
            case Input.Keys.NUM_2:
                clickOnPole(poles.get(1));
                break;
            case Input.Keys.NUM_3:
                clickOnPole(poles.get(2));
                break;
            case Input.Keys.ESCAPE:
                game.setScreen(new TitleScreen(game));
                this.dispose();
            case Input.Keys.SPACE:
                if (!switchGameState()) {
                    clickOnBackground();
                }
                break;
        }
        updateState();
    }


    public enum Platform {
        PC, ANDROID, DEFAULT;
    }

    private enum GameState {
        PAUSED, RUNNING, FINISHED, NOT_STARTED;
    }

    private class DesktopInputHandler implements InputProcessor {

        @Override
        public boolean keyDown(int keycode) {
            performKeyStrokes(keycode);
            return true;
        }

        @Override
        public boolean keyUp(int keycode) {
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            return false;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            performClicks(screenX, screenY);
            return false;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            return false;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            return false;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }

        @Override
        public boolean scrolled(int amount) {
            return false;
        }
    }

    private class PhoneInputHandler implements GestureDetector.GestureListener {

        @Override
        public boolean touchDown(float x, float y, int pointer, int button) {
            performClicks(x, y);
            return true;
        }

        @Override
        public boolean tap(float x, float y, int count, int button) {
            return false;
        }

        @Override
        public boolean longPress(float x, float y) {
            switchGameState();
            return true;
        }

        @Override
        public boolean fling(float velocityX, float velocityY, int button) {
            return false;
        }

        @Override
        public boolean pan(float x, float y, float deltaX, float deltaY) {
            return false;
        }

        @Override
        public boolean panStop(float x, float y, int pointer, int button) {
            return false;
        }

        @Override
        public boolean zoom(float initialDistance, float distance) {
            return false;
        }

        @Override
        public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
            return false;
        }

        @Override
        public void pinchStop() {
        }
    }
}
