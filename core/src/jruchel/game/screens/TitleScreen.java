package jruchel.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import jruchel.game.MainGame;
import jruchel.game.rendering.RenderableText;

public class TitleScreen implements Screen {


    private MainGame game;
    private RenderableText titleText;

    //Variables
    private int rings = 4;
    private float width;
    private float height;

    public TitleScreen(MainGame game) {
        this.game = game;
        this.width = game.getScreenWidth();
        this.height = game.getScreenHeight();
    }


    private RenderableText createTitleText() {
        FreeTypeFontGenerator.FreeTypeFontParameter fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontParameter.size = (int) (height / 20);
        fontParameter.color = Color.BLACK;
        return titleText = new RenderableText(fontParameter, game.getTextPrefs().get("selectRings"), 0, game.getScreenHeight());
    }

    @Override
    public void show() {
        game.resetRenderer();
        titleText = createTitleText();
        game.addRenderable(titleText);
        Gdx.input.setInputProcessor(new InputProcessor() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode >= 10 && keycode <= 16) {
                    game.setScreen(new GameScreen(game, keycode - 7));
                }
                else if (keycode >= 147 && keycode <= 153) {
                    game.setScreen(new GameScreen(game, keycode - 144));
                }
                return true;
            }

            @Override
            public boolean keyUp(int keycode) {
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return true;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return false;
            }
        });
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.renderTextures();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
