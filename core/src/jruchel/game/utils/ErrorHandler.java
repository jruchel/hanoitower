package jruchel.game.utils;

public interface ErrorHandler {

    void throwException(Exception ex);
}
