/*
package jruchel.game.bots;

import java.util.List;

import jruchel.game.MainGame;
import jruchel.game.models.Pole;

public class HanoiTowerBot implements Runnable {

    private final MainGame game;
    private List<Pole> poles;
    private int moves = 0;

    public HanoiTowerBot(MainGame game) {
        this.game = game;
        poles = game.getPoles();
    }

    private void playGame() throws InterruptedException {
        while (!game.isOver()) {
            solve(game.getRings(), poles.get(0), poles.get(1), poles.get(2));
        }
        game.clickOnBackground();
    }

    private void solve(int a, Pole from, Pole aux, Pole to) throws InterruptedException {
        if (a == 1) {
            synchronized (this) {
                wait(1000);
            }
            game.clickOnPole(from);
            game.clickOnPole(to);
            moves++;
        } else {
            solve(a - 1, from, to, aux);
            synchronized (this) {
                wait(1000);
            }
            game.clickOnPole(from);
            game.clickOnPole(to);
            moves++;
            solve(a - 1, aux, from, to);
        }
    }

    public int getMoves() {
        return moves;
    }

    @Override
    public void run() {
        poles = game.getPoles();
        try {
            playGame();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}*/
