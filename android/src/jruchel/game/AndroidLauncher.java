package jruchel.game;

import android.os.Bundle;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import java.util.HashMap;
import java.util.Map;

import jruchel.game.output.AlertDisplayer;

public class AndroidLauncher extends AndroidApplication {

    private final MainGame game = new MainGame();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initializeTextPrefs();

        game.setAlertDisplayer(new AlertDisplayer() {
            @Override
            public void display(final String msg) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(AndroidLauncher.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        initialize(game, config);
    }

    void initializeTextPrefs() {
        Map<String, String> temp = new HashMap<>();
        temp.put("gameOver", getString(R.string.gameOver));
        temp.put("moves", getString(R.string.moves));
        temp.put("highScore", getString(R.string.highScore));
        temp.put("time", getString(R.string.time));
        temp.put("newHS", getString(R.string.newHS));
        temp.put("HSSaved", getString(R.string.HSSaved));

        game.setTextPrefs(temp);
    }
}
